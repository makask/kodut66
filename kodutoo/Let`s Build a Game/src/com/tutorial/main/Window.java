package com.tutorial.main;

import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Window extends Canvas {

	private static final long serialVersionUID = -240840600533728354L;

	public Window(int width, int height, String title, Game game) {
		JFrame frame = new JFrame(title); // The frame of our window
		
		frame.setPreferredSize(new Dimension(width, height)); // Saying preferred size of the window
		frame.setMaximumSize(new Dimension(width, height));	  // Maximum size of the window
		frame.setMinimumSize(new Dimension(width, height));	  // Minimum size of the window
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Acts as a exit button, closes your program after exit
		frame.setResizable(false); 	// Can We resize our window. 
		frame.setLocationRelativeTo(null); // Window start in the middle of a screen. 
		frame.add(game); // Adding game class to a frame
		frame.setVisible(true);	// Setting frame visible
		game.start();	// Running start method
	}

}
